package com.game.blackjack;

import com.game.domain.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Touqir on 14/10/2015.
 */
public class BlackjackGameTest {

    private BlackjackGame game;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldCreateDefaultNumberOfPlayersWhenPlayerParameterValueIsNotInValidRange() throws Exception {
        // when
        BlackjackGame blackjackGame = new BlackjackGame(null, null);

        // Then
        assertEquals(3, blackjackGame.getCurrentNumberOfPlayers());

    }

    @Test
    public void shouldCreateSameNumberOfPlayersWhenPlayerParameterValueIsInValidRange() throws Exception {
        // Given
        List<Player> players = Player.createPlayers(2);
        // when
        BlackjackGame blackjackGame = new BlackjackGame(players, null);

        // Then
        assertEquals(2, blackjackGame.getCurrentNumberOfPlayers());

    }

}