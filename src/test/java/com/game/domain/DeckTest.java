package com.game.domain;

import com.game.shufflealgorithm.DefaultShuffle;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Touqir on 14/10/2015.
 */
public class DeckTest {

    private Deck deck;

    @Before
    public void setUp() throws Exception {
        deck = new Deck(new DefaultShuffle());
    }

    @Test
    public void whenCardIsDrawnThenSizeShouldDecrease() throws Exception {
        //Given
        int previousSize = deck.size();

        // when
        deck.drawCard();

        // then
        assertEquals(52, previousSize);
        assertEquals(51, deck.size());
    }
}