package com.game.domain;

import com.game.ShuffleMechanism;
import com.game.exception.NoMoreCardsException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is the domain class that represents a deck of cards in a card game.
 *
 * Created by Touqir on 13/10/2015.
 */
public class Deck {

	public static final int TOTAL_CARDS_PER_DECK = 52;
	public static final int DEFAULT_NUMBER_OF_PACKS = 1;

	private ShuffleMechanism shuffleMechanism;
	private List<Card> cards;
	private int topCardIndex;
	private int numberOfPacks;

	public Deck(ShuffleMechanism shuffleMechanism) {
		this(1, shuffleMechanism);
	}

	public Deck(int numberOfPacks, ShuffleMechanism shuffleMechanism) {
		this.numberOfPacks = numberOfPacks <= 0 ? DEFAULT_NUMBER_OF_PACKS : numberOfPacks;
		this.shuffleMechanism = shuffleMechanism;
		initCardDeck();
	}

	protected void initCardDeck() {
		final int totalCards = TOTAL_CARDS_PER_DECK * numberOfPacks;
		cards = new ArrayList<Card>(totalCards);
		topCardIndex = totalCards - 1;

		for(int i = 0; i < numberOfPacks; i++) {
			for (CardSuit suit : CardSuit.values() ) {
				for (CardName cardName : CardName.values())  {
					Card c = new Card(cardName, suit);
					cards.add(c);
				}
			}
		}
	}

	public void setShuffleMechanism(ShuffleMechanism shuffleMechanism) {
		this.shuffleMechanism = shuffleMechanism;
	}

	public void shuffle() {
		shuffleMechanism.shuffle(cards);
	}

    public Card drawCard() {
		if(topCardIndex < 0)
			throw new NoMoreCardsException("No more cards left in deck");

    	return cards.get(topCardIndex--);
    }

	public List<Card> drawCards(int numberOfCards) {
		if(numberOfCards <= 0)
			return Collections.emptyList();

		List<Card> cards = new ArrayList<Card>(numberOfCards);
		for(int i = 0; i < numberOfCards; i++) {
			Card topCard = drawCard();
			cards.add(topCard);
		}

		return cards;
	}

	public int size() {
		return topCardIndex + 1;
	}

}
