package com.game.domain;

import com.game.exception.NoMoreCardsException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is the domain class that represents a player in a card game.
 *
 * Created by Touqir on 13/10/2015.
 */
public class Player {

    private List<Card> cards = new ArrayList<Card>();
    private String name;
    private PlayerStatus status;
	private int totalValue;
    
    public Player(String playerName) {
    	this.name = playerName;
    	status = PlayerStatus.HIT;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlayerStatus getStatus() {
		return status;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public int getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(int totalValue) {
		this.totalValue = totalValue;
	}

	public void dealCard(Card card) {
		if(card == null) return;

    	cards.add(card);
    }

	public void dealCards(List<Card> multipleCards) {
		if(multipleCards == null || multipleCards.isEmpty())
			return;

		cards.addAll(multipleCards);
	}

	public boolean canPlay() {
		return status == PlayerStatus.HIT;
	}
    
    public Card playCard() {
		if(! isCardsExist())
			throw new NoMoreCardsException("No more cards left with player[" + name + "]");

		return cards.remove(cards.size() - 1);
    }

    public boolean isCardsExist() {
    	return cards.size() > 0;
    }

	public static List<Player> createPlayers(int numPlayers) {
		if(numPlayers < 1)
			return Collections.emptyList();

		List<Player> players = new ArrayList<Player>(numPlayers);

		for(int i = 1; i <= numPlayers; i++) {
			players.add(new Player("player" + i));
		}

		return players;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Player player = (Player) o;

		if (cards != null ? !cards.equals(player.cards) : player.cards != null) return false;
		if (name != null ? !name.equals(player.name) : player.name != null) return false;
		return status == player.status;

	}

	@Override
	public int hashCode() {
		int result = cards != null ? cards.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (status != null ? status.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", status=" + status.name() + "]";
	}
 
}