package com.game.domain;

public enum CardSuit { CLUB, SPADE, HEART, DIAMOND }
