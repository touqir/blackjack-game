package com.game.domain;

/**
 * This is the domain class that represents a card in a card game.
 *
 * Created by Touqir on 13/10/2015.
 */
public class Card {
	private final CardName cardName;
	private final CardSuit suit;

	public Card(CardName cardName, CardSuit suit) {
		this.cardName = cardName;
		this.suit = suit;
	}

	public CardName getCardName() {
		return cardName;
	}

	public CardSuit getSuit() {
		return suit;
	}

	public int getCardValue() {
		return cardName.getValue();
	}

	@Override
	public String toString() {
		return "[" + cardName + " of " + suit + "]";
	}
	

}
