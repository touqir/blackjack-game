package com.game.domain;

public enum CardName {
	Ace(11),
	TWO(2),
	Three(3),
	Four(4),
	Five(5),
	Six(6),
	Seven(7),
	Eight(8),
	Nine(9),
	Ten(10),
	Jack(10),
	Queen(10),
	King(10);

	private int value;

	private CardName(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
