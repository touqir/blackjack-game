package com.game;

import com.game.domain.Card;

import java.util.List;

/**
 * Created by Touqir on 13/10/2015.
 */
public interface ShuffleMechanism {
    public void shuffle(List<Card> deckOfCards);
}
