package com.game.exception;

/**
 * Created by Touqir on 13/10/2015.
 */
public class NoMoreCardsException extends  RuntimeException {

    public NoMoreCardsException(String message) {
        super(message);
    }
}
