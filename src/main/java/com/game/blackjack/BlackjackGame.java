package com.game.blackjack;

import com.game.Game;
import com.game.domain.Card;
import com.game.domain.Deck;
import com.game.domain.Player;
import com.game.domain.PlayerStatus;
import com.game.exception.NoMoreCardsException;
import com.game.shufflealgorithm.DefaultShuffle;

import java.util.List;
import java.util.Scanner;

/**
 * Class that handles the playing of a blackjack game from a simple command line interface,
 * and echoes back a step-by-step description of the game to the console.
 *
 * Created by Touqir on 13/10/2015.
 */
public class BlackjackGame implements Game {

	public static final int DEFAULT_NUMBER_OF_PLAYERS = 3;
	public static final int MAX_PLAYERS = 6;
	public static final int MIN_PLAYERS = 2;

	private List<Player> players;
	private Deck deck;
	private Player winner;

	public BlackjackGame(List<Player> players, Deck deck) {
		setPlayers(players);
		setDeck(deck);
	}

	public void setPlayers(List<Player> players) {
		if(players == null || players.size() < MIN_PLAYERS || players.size() > MAX_PLAYERS) {
			this.players = Player.createPlayers(DEFAULT_NUMBER_OF_PLAYERS);
		} else {
			this.players = players;
		}
	}

	public void setDeck(Deck deck) {
		this.deck = (deck == null) ? new Deck(new DefaultShuffle()) : deck;
	}

	public int getCurrentNumberOfPlayers() {
		return players.size();
	}

	public Player getWinner() {
		return winner;
	}

	public void start() {
		// shuffle deck
		deck.shuffle();

		// deal two cards to each player
		dealHandOfTwoCardsToEachPlayer();

		// allow each player to play until a winner is found
		while(winnerNotFound() && playersAreMoreThanOne() && allPlayersAreNotSticking() && deckNotEmpty()) {

			for (int index = 0; index < players.size(); index++) {
				Player player = players.get(index);

				// if player has just stuck, just continue to the next player
				if (!player.canPlay()) continue;

				Card cardPlayed = player.playCard();
				log(player + " has played this card - " + cardPlayed);

				final int playerTotalValue = player.getTotalValue() + cardPlayed.getCardValue();
				player.setTotalValue(playerTotalValue);

				// check against game rules
				if (playerTotalValue > 21) {
					player.setStatus(PlayerStatus.BURST);
					log(player.getName() + " has bursted.");
					players.remove(index);
					index--; // reverse index value after removing player from list
					continue;

				} else if (playerTotalValue == 21) {
					player.setStatus(PlayerStatus.WINNER);
					winner = player;
					break;

				} else if (playerTotalValue < 17) {
					player.setStatus(PlayerStatus.HIT);

				} else {
					player.setStatus(PlayerStatus.STICK);
					log(player.getName() + " is stick now.");
				}

				// deal next card to player after he played his card
				try {
					dealNextCardToPlayer(player);
				} catch (NoMoreCardsException e) {
					log("Card deck has finished");
					break;
				}

			} // for loop end

		} // while loop end

		// determin winner now if not found
		if(winnerNotFound()) {
			winner = findWinner();
		}

		//display winner
		log("Winner is = " + winner);
	}

	private boolean winnerNotFound() {
		return winner == null;
	}


	private void dealHandOfTwoCardsToEachPlayer() {
		for (Player player : players) {
			player.dealCards(deck.drawCards(2));
			log("2 cards has been dealt to  " + player);
		}
	}


	private void dealNextCardToPlayer(Player player) {
		if(player.canPlay()) {
			Card cardDrawn = deck.drawCard();
			player.dealCard(cardDrawn);
			log(player + " has been dealt a new card from deck - " + cardDrawn);
		}
	}

	private boolean playersAreMoreThanOne() {
		return players.size() > 1;
	}

	private boolean allPlayersAreNotSticking() {
		for(Player player : players) {
			if(player.canPlay()) {
				return true;
			}
		}

		return false;
	}

	private boolean deckNotEmpty() {
		return deck.size() > 0;
	}

	private Player findWinner() {
		Player winner = null;

		int maxTotalValue = 0;
		for(Player player : players) {
			if(maxTotalValue < player.getTotalValue()) {
				maxTotalValue = player.getTotalValue();
				winner = player;
			}
		}

		return winner;
	}

	private static void log(String message) {
		System.out.println(message);
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Number of players:");
		int numPlayers = scanner.nextInt();

		List<Player> players = Player.createPlayers(numPlayers);
		Deck deck = new Deck(new DefaultShuffle());
		Game game = new BlackjackGame(players, deck);

		game.start();
	}
}
