package com.game.shufflealgorithm;

import com.game.ShuffleMechanism;
import com.game.domain.Card;

import java.util.Collections;
import java.util.List;

/**
 * Created by Touqir on 13/10/2015.
 */
public class DefaultShuffle implements ShuffleMechanism {

    @Override
    public void shuffle(List<Card> deckOfCards) {
        Collections.shuffle(deckOfCards);
    }
}
